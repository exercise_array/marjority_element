import java.util.HashMap;
import java.util.Map;

public class Main {

    public static int marjoritySimple(int[] arr, int n) {
        for (int i = 0; i < n; i++) {
            int count = 0;
            for (int j = 0; j < n; j++) {
                if (arr[i] == arr[j])
                    count++;
            }
            if (count >= n/2) {
                return i;
            }
        }
        return -1;
    }

    public static int marjorMooreVoting(int[] arr, int n) {
        int index = 0, count =1;
        for (int i = 1; i < n; i++) {
            if (arr[i] == arr[index])
                count++;
            else count--;
            if (count == 0) {
                count = 1;
                index = i;
            }
        }
        return arr[index];
    }

    public static boolean isMarjor(int[] arr, int n, int elemment) {
        int count = 0;
        for(int i = 0; i < n; i++) {
            if (arr[i] == elemment)
                count++;
        }
        if (count >= n / 2 )
            return true;
        return false;
    }

    public static void printMarjorMooreVoting(int[] arr, int n) {
        int mooreVoting = marjorMooreVoting(arr, n);
        if (isMarjor(arr, n, mooreVoting)) {
            System.out.println(mooreVoting);
        } else {
            System.out.println("Khong ton tai");
        }
    }

    public static void marjorityHashMap(int[] arr, int n) {
        Map<Integer, Integer> map = new HashMap<>();
        for(int i = 0; i < arr.length; i++) {
            if (map.containsKey(arr[i])) {
                int count = map.get(arr[i]) +1;
                if (count >= arr.length /2) {
                    System.out.println(arr[i]);
                    return;
                } else
                    map.put(arr[i], count);

            }
            else
                map.put(arr[i],1);
        }
        System.out.println("Khong ton tai");

    }
    public static void main(String[] args) {
	    int[] arr = {1, 2, 4, 4, 3, 4, 5, 4};
	    int pos = marjoritySimple(arr, arr.length);
        System.out.println(pos == -1 ? "Khong ton tai": arr[pos]);
        printMarjorMooreVoting(arr, arr.length);
        marjorityHashMap(arr, arr.length);
    }
}
